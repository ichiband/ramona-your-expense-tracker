import { GlobalService } from './../global.service';
import { Component, OnInit } from '@angular/core';
import {
  FormGroup,
  FormControl,
  ReactiveFormsModule,
  FormBuilder,
  Validators
} from "@angular/forms";
import { MatSnackBar } from '@angular/material';
import { NgxNavigationWithDataComponent } from 'ngx-navigation-with-data';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  name = "";
  totalPrice = "";
  imageData = {};
  imageUrl = "";
  id = "";
  constructor(public fb: FormBuilder, private snackBar: MatSnackBar, public global: GlobalService, public navCtrl: NgxNavigationWithDataComponent) {
    console.log(this.navCtrl.data); 
  }
  
  removeImage() {
    this.imageUrl = "";
    this.imageData = {};
  }

  ngOnDestroy(): void {

  }

  ngOnInit() {
    if(this.navCtrl.get('data')) {
      this.name = this.navCtrl.get('data').name;
      this.totalPrice = this.navCtrl.get('data').total_price;
      this.imageUrl = this.navCtrl.get('data').image_url;
      this.id = this.navCtrl.get('data').id;
    }
  }

  //to add expenses 
  submit() {
    console.log(this.global.userDetails);
    if(this.name == '') {
      this.global.openSnackBar("Please enter the name of expense", "ok");
    } else if(this.totalPrice == '' || !this.totalPrice.match(/^-?\d*(\.\d+)?$/)) {
      this.global.openSnackBar("Please enter correct total price of expense", "ok");
    } else {
      var obj = {user_id:this.global.userDetails['id'],name:this.name, totalPrice: this.totalPrice, imageData: this.imageData};
      if(this.id != '') {
        obj['id'] = this.id;
        obj['url'] = this.imageUrl;
      }
      this.global.post(this.global.serverUrl+"add_expense.php", `expenseJson=${JSON.stringify(obj)}`, data=>{
        console.log(data);
        if(data.response) {
          this.name = "";
          this.totalPrice = "";
          this.imageData = {};
          this.imageUrl = "";
          this.global.openSnackBar("Expense has been recorded, thank you.", "ok");
        }
      }, err=>{});
    }

  }

  //for updating image file
  changeOnFile(event) {
    var file = event.target.files[0];
    this.imageData['fileName']=file['name'];
    var reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      var data = "";
      data = reader.result['split'](",")[1];
      data.replace(" ", "");
      this.imageData['fileData'] = data;
    };
    console.log(this.imageData);
  }

}
