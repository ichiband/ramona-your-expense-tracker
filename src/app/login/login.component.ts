import { Component, OnInit } from '@angular/core';
import { AuthService,
    FacebookLoginProvider,
    GoogleLoginProvider
} from 'angular5-social-login';
import { GlobalService } from '../global.service';
import { NgxNavigationWithDataComponent } from 'ngx-navigation-with-data';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  dataToBeSend:{
    id: any;
    email: any;
    name: any;
    profile: any;
  }

  adminEmail: any;
  adminPassword: any;

  constructor(public navCtrl: NgxNavigationWithDataComponent, private socialAuthService: AuthService, public global: GlobalService) {
    this.checkUser();
  }

checkUser() {
  if(this.global.userDetails['id']) {
    this.navCtrl.navigate('home');
  }
  if(this.global.getLocalStorage('userDetails')) {
    this.global.userDetails = this.global.getLocalStorage('userDetails');
    this.navCtrl.navigate('table-list');
  } else {
    this.navCtrl.navigate('login');
  }
}

//for new user, redirect them to create new account
sendToCreateAccount() {
  this.navCtrl.navigate('create-account');
}

ngOnInit() {
}
//save user data for session management
  saveUserData(userData){
    console.log("save user data");
    var body=`userData=${JSON.stringify(userData)}`;
    // console.log(body);
    this.global.post(this.global.serverUrl + 'user_login.php', body, (data) => {
        this.global.setGlobalUserdetails(userData);
        this.navCtrl.navigate('home');
    }, function(err){
    }, false);
  }
  
  //login user by checking existing credentials in db & then redirect him to his/her account
  login(){
    var body='username='+this.adminEmail+'&password='+this.adminPassword;
    this.global.post(this.global.serverUrl + 'user_login.php', body, (data) => {
      if(data.response) {
        this.global.setGlobalUserdetails(data.data);
        this.navCtrl.navigate('');
      } else {
        alert('please enter correct password and username');
      }
    }, function(err){
    }, false);
  }
    

}
