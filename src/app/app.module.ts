import { GlobalService } from './global.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule, FormGroup} from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';


import { AppRoutingModule } from './app.routing';
import { ComponentsModule } from './components/components.module';

import { AppComponent } from './app.component';

import { DashboardComponent } from './dashboard/dashboard.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { TableListComponent } from './table-list/table-list.component';
import { TypographyComponent } from './typography/typography.component';
import { IconsComponent } from './icons/icons.component';
import { MapsComponent } from './maps/maps.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { UpgradeComponent } from './upgrade/upgrade.component';
import { SocialLoginModule, AuthServiceConfig, GoogleLoginProvider, FacebookLoginProvider } from "angular5-social-login";
import { AgmCoreModule } from '@agm/core';
import { MatTableModule } from "@angular/material/table";
import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';
import { MatButtonModule, MatCheckboxModule, MatSortModule,MatPaginatorModule, MatSnackBarModule } from '@angular/material';
import { NgxNavigationWithDataComponent } from 'ngx-navigation-with-data';
import { LoginComponent } from './login/login.component';
import { CreateAccountComponent } from './create-account/create-account.component';
import {NgxPaginationModule, PaginatePipe} from 'ngx-pagination';

export function getAuthServiceConfigs() {
  let config = new AuthServiceConfig(
      [
        {
          id: FacebookLoginProvider.PROVIDER_ID,
          provider: new FacebookLoginProvider("2153582588292788")
        },
        {
          id: GoogleLoginProvider.PROVIDER_ID,
          provider: new GoogleLoginProvider("605152719308-1aqicjke7t3qvbotpean6oa93quomi0a.apps.googleusercontent.com")
        }
      ]
  );
  return config;
}
@NgModule({
  imports: [NgxPaginationModule,
    MatSnackBarModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatCheckboxModule,
    SocialLoginModule,
    MatSortModule,
    FormsModule,
    MatPaginatorModule,
    MatTableModule,
    HttpModule,
    ComponentsModule,
    RouterModule,
    AppRoutingModule,
    ReactiveFormsModule,
    AgmCoreModule.forRoot({
      apiKey: 'YOUR_GOOGLE_MAPS_API_KEY'
    })
  ],
  declarations: [
    AppComponent,
    AdminLayoutComponent,
    LoginComponent,
    CreateAccountComponent
  ],
  providers: [{
      provide: AuthServiceConfig,
      useFactory: getAuthServiceConfigs
    },GlobalService, NgxNavigationWithDataComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
