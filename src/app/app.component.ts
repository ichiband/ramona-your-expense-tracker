import { GlobalService } from './global.service';
import { Component} from '@angular/core';
import { NgxNavigationWithDataComponent } from 'ngx-navigation-with-data';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(public global: GlobalService, public navCtrl: NgxNavigationWithDataComponent) {
    this.checkLogin();
  }

  //to ensure the login session stays continue untill unless user wants to logout
  checkLogin() {
    if(this.global.getLocalStorage('userDetails')) {
      this.global.userDetails = this.global.getLocalStorage('userDetails');
      this.navCtrl.navigate('table-list');
    } else {
      this.navCtrl.navigate('login');
    }
  }
}
