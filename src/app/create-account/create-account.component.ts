import { GlobalService } from './../global.service';
import { Component, OnInit } from '@angular/core';
import { NgxNavigationWithDataComponent } from 'ngx-navigation-with-data';

@Component({
  selector: 'app-create-account',
  templateUrl: './create-account.component.html',
  styleUrls: ['./create-account.component.scss']
})
export class CreateAccountComponent implements OnInit {
  name = "";
  emailId="";
  password= "";
  confirmPassword = "";
  constructor(public global: GlobalService,public navCtrl: NgxNavigationWithDataComponent) { }

  ngOnInit() {
  }
//to create account after entering required details
  submit() {
    if(this.name == '') {
      this.global.openSnackBar("Please enter the name", "ok");
    } else if(this.emailId == '' || !/^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/.test(this.emailId)) { //email validation
      this.global.openSnackBar("Please enter correct email id", "ok");
    } else if(this.password == '') {
      this.global.openSnackBar("Please enter password", "ok");
    } else if(this.confirmPassword != this.password) {
      this.global.openSnackBar("Your passwords do not match", "ok");
    } else {
      var obj = {id:this.global.createGuid(),name:this.name, email: this.emailId, password: this.password};
      this.global.post(this.global.serverUrl+"user_login.php", `userData=${JSON.stringify(obj)}`, data=>{
        if(data.response) {
          this.global.userDetails = {id:obj.id, name:obj.name, email: obj.email};
          this.global.setLocalStorage('userDetails', {id:obj.id, name:obj.name, email: obj.email});
          this.navCtrl.navigate('table-list');
        }
      }, err=>{});
    }
  }


}
