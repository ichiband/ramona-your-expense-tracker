import { MatSnackBar } from '@angular/material';
import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Router } from "@angular/router";
import 'rxjs/add/operator/map';
@Injectable()
export class GlobalService {
  imageUrl = "http://13.233.75.137/ramonab/images/";
  lists = {};
  lists_values = {};
  userDetails = {};
  cities = [];
  serverUrl = "http://13.233.75.137/ramonab/";
  constructor(public snackBar: MatSnackBar, public http: Http, public router: Router) {
  }


  public post(url, body, successCallback, failedCallback, loader = true, text = "Please wait", headerString = '') {
    var callbackCalled = false;
    var timeInterval = [15000, 30000, 60000, 75000];
    var timeOut = [];
    for (let i = 0; i < timeInterval.length; i++) {
      const time = timeInterval[i];
      var netSlowTimout = setTimeout(() => {
        if (i != 0)
          clearTimeout(timeOut[i - 1]);
      }, time);
      timeOut.push(netSlowTimout);
    }
    var headers = new Headers({
      "Content-Type": "application/x-www-form-urlencoded"
    });
    var options = new RequestOptions({
      headers: headers
    });
    var isLoader: boolean = loader;
    this.http.post(url, body, options).map(res => res.json()).subscribe(
      data => {
        // Check pool close error and tell user
        if (!callbackCalled) {
          callbackCalled = true;
          successCallback(data);
        }
      }, err => {
        clearTimeout(netSlowTimout);
        failedCallback(err);
      });
  }


  public get(url, successCallback, failedCallback, loader = true, text = 'Please wait...') {
   
    this.http
      .get(url).map(res => res.json())
      .subscribe(
        data => {
         
          successCallback(data);
        },
        err => {
          failedCallback(err);
        }
      );
  }

  numberOnlyValidation(event: any) {
    const pattern = /[0-9]/;
    let inputChar = String.fromCharCode(event.charCode);
    console.log(inputChar);
    console.log(typeof inputChar);

    if (!pattern.test(inputChar)) {
      console.log("invalid");
      // invalid character, prevent input
      event.preventDefault();
    }
  }
  
  setLocalStorage(key, value) {
    localStorage.setItem(key, JSON.stringify(value))
  }
  getLocalStorage(key) {
    return JSON.parse(localStorage.getItem(key));
  }
  setGlobalUserdetails(data) {
    this.setLocalStorage('userDetails', data);
    this.userDetails = data;
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }

  createGuid() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
            const r = Math.random() * 16 | 0, v = c === 'x' ? r : ( r & 0x3 | 0x8 );
            return v.toString(16);
        });
  }

}
