import { GlobalService } from './../global.service';
import { Component, OnInit } from '@angular/core';
import { NgxNavigationWithDataComponent } from 'ngx-navigation-with-data';
@Component({
  selector: 'app-table-list',
  templateUrl: './table-list.component.html',
  styleUrls: ['./table-list.component.css']
})
export class TableListComponent implements OnInit {
  expenseData = [];
  tempData = [];
  totalExpense:any;
  sortObj = {field:"", flag:false};
  typAheadValue = "";
  constructor(public global: GlobalService, public navctrl: NgxNavigationWithDataComponent) {
    this.getAllExpense();
  }

  ngOnInit() {
  }
//fetch all expenses for the respective user logged in
  getAllExpense() {
    this.global.post(this.global.serverUrl+ "get_all_expense.php", `user_id=${this.global.userDetails['id']}`, data=>{
      this.tempData = data.data;
      this.expenseData = data.data;
      this.totalExpense += data.data.total_price;
      console.log(this.totalExpense);
    },err=>{})
  }
//custom sorting for the fields in the table
  sort(field) {
    this.sortObj.field = field;
    if(this.sortObj.flag) {
      this.expenseData.sort((a,b) => (a[field] > b[field]) ? 1 : ((b[field] > a[field]) ? -1 : 0));
    } else {
      this.expenseData.sort((a,b) => (a[field] < b[field]) ? 1 : ((b[field] < a[field]) ? -1 : 0));
    }
    this.sortObj.flag = !this.sortObj.flag;
  }

  //for editing the expenses
  updateExpense(each) {
    this.navctrl.navigate('dashboard', {data: each});
  }

  //for deleting any existing expense
  deleteExpense(each, i) {
    var a = confirm("Are you sure?");
    if(a) {
      this.global.post(this.global.serverUrl + "remove_expense.php", `id=${each.id}`, data=>{
        this.expenseData.splice(i,1);
      }, err=>{});
    }
  }

  //for searching a particular expense
  search(){
    this.expenseData = this.tempData.filter((eachData)=>{
      if(eachData['name'].indexOf(this.typAheadValue) != -1 || eachData['total_price'].indexOf(this.typAheadValue) != -1 || eachData['date_time'].indexOf(this.typAheadValue) != -1)
      return eachData;
    });
  }

}
